import os
import shutil
import subprocess
from metakernel import MetaKernel, Magic
import sys
from tempfile import TemporaryDirectory
from pkg_resources import resource_filename

class DefinitionMagic(Magic):
    def cell_def(self):
        self.kernel.cpp_parts['def'] = self.code
        self.evaluate = None

class CppKernel(MetaKernel):
    implementation = 'C++ Kernel'
    implementation_version = '1.0'
    language = 'c++'
    language_version = '?'
    language_info = {'name': 'c++',
                     'version': '?',
                     'codemirror_mode': 'c++',
                     'mimetype': 'text/x-c++src',
                     'file_extension': '.c++'}
    banner = 'C++ Kernel'
    kernel_json = {
        'argv': [ sys.executable, '-m', 'cpp_kernel', '-f', '{connection_file}'],
        'display_name': 'C++',
        'language': 'c++',
        'name': 'cpp_kernel'
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.tmp_dir = TemporaryDirectory()
        self.cmake_source_dir = self.tmp_dir.name + '/src'
        self.cmake_build_dir = self.tmp_dir.name + '/build'
        self.cpp_parts = {'def': '', 'exe': '', 'printer': ''}
        with open(resource_filename('cpp_kernel', 'cpp_buildfiles/main.cc.template')) as f:
            self.cpp_main_template = f.read()
        self._setup_buildfiles()
        self.register_magics(DefinitionMagic)

    def _run_process(self, args):
        proc = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.log.debug(proc.stdout.decode('utf-8'))
        self.log.debug(proc.stderr.decode('utf-8'))
        return proc

    def _setup_buildfiles(self):
        shutil.copytree(resource_filename('cpp_kernel', 'cpp_buildfiles'), self.cmake_source_dir)
        os.makedirs(self.cmake_build_dir)
        with open(self.cmake_source_dir + '/main.cc', 'w') as f:
            f.write(self._compose_cpp_source(''))
        self._run_process(['cmake', '-S', self.cmake_source_dir, '-B', self.cmake_build_dir])
        self._run_process(['cmake', '--build', self.cmake_build_dir])

    def _compose_cpp_source(self, code):
        exes = code.strip().splitlines()
        printer = ''
        if len(exes) >= 1 and not exes[-1].endswith(';'):
            printer = 'std::cout << ({}) << std::endl;'.format(exes.pop())
        self.cpp_parts.update({ 'exe': '\n'.join(exes), 'printer': printer })
        return self.cpp_main_template.format(**self.cpp_parts)

    def _compile_and_execute_cpp_source(self, src):
        with open(self.cmake_source_dir + '/main.cc', 'w') as f:
            f.write(src)
        proc = self._run_process(['cmake', '--build', self.cmake_build_dir])
        if proc.returncode != 0: # Compile error
            return {'name': 'stderr', 'text': proc.stderr.decode('utf-8') + '~'*80 + '\n' + src}
        else:
            proc = self._run_process([self.cmake_build_dir + '/main'])
            if proc.returncode != 0: # Runtime error
                return {'name': 'stderr', 'text': proc.stderr.decode('utf-8') + '~'*80 + '\n' + src}
            else:
                return {'name': 'stdout', 'text': proc.stdout.decode('utf-8')}

    # Metakernel API
    def do_execute_direct(self, code, silent=False):
        src = self._compose_cpp_source(code)
        result = self._compile_and_execute_cpp_source(src)
        self.send_response(self.iopub_socket, 'stream', result)

    # Kernel API
    def do_shutdown(self, restart):
        self.tmp_dir.cleanup()
        return {'status': 'ok', 'restart': restart}
