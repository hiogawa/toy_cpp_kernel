import jupyter_kernel_test


class CppKernelTests(jupyter_kernel_test.KernelTests):
  maxDiff = None
  kernel_name = 'cpp_kernel'

  def _execute(self, code):
      self.flush_channels()
      reply, output_msgs = self.execute_helper(code=code)
      return output_msgs[0]['content']['text']

  def test_simple(self):
    test_cases = [
      ('"hello, world"', 'hello, world\n'),
      ('1 + 2 + 3', '6\n'),
      ('1 + 2 + 3;\n', '')
    ]
    for case in test_cases:
      self.assertEqual(self._execute(case[0]), case[1])


  def test_1(self):
    code_def = """\
%%def
#include <cstdio>
"""
    code_main = """
std::printf("hello, world\\n");
"""
    result = "hello, world\n"
    self._execute(code_def)
    self.assertEqual(self._execute(code_main), result)


  def test_2(self):
    code_def = """\
%%def
#include <cmath>
using namespace std;

int GcdRec(int a, int b) {
  if (a == 0) {
    return b;
  } else {
    return GcdRec(b % a, a);
  }
}
int Gcd(int a, int b) {
  a = abs(a);
  b = abs(b);
  return GcdRec(min(a, b), max(a, b));
}
"""
    code_main = """
int a = pow(2, 16) - 1;
int b = 17 * 5;
Gcd(b, a)
"""
    result = str(17 * 5) + '\n'
    self._execute(code_def)
    self.assertEqual(self._execute(code_main), result)


if __name__ == '__main__':
  import unittest
  unittest.main()
