# Installation

```
$ pip install .
$ python -m cpp_kernel install

# Or for development and testing
$ pip install -e .[dev]
$ python -m cpp_kernel.test

# Or for conda/virtual environment
$ jupyter kernelspec install --sys-prefix
```

# Dependencies

- cmake, clang++

# Todo

- Support custom cmake script
- Help using data from cppreference.com
