from setuptools import setup, find_packages

setup(
    name="cpp_kernel",
    version="0.1",
    packages=find_packages(),
    install_requires=['metakernel'],
    extras_require={
        'dev': ['jupyter_kernel_test']
    },
    include_package_data=True,
)
